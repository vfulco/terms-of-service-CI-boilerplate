TEX = pandoc
FLAGS = --latex-engine=xelatex
BUILDDIR = public

clsrc := $(filter-out README.md, $(wildcard *.md))
CLTARGETS := $(patsubst %.md,$(BUILDDIR)/%.pdf,$(clsrc))
CLTEMPLATE := tostemplate.tex

all: $(CLTARGETS)

$(CLTARGETS): $(BUILDDIR)/%.pdf : %.md $(CLTEMPLATE)
	$(TEX) $< --verbose -o $@ --template=$(CLTEMPLATE) $(FLAGS)

.PHONY: clean
clean :
	rm public/*.pdf
